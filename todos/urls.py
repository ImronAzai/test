from django.urls import path
from todos.views import view_todolist, view_task, create_view, edit_list
from todos.views import create_task, delete_list, update_item

urlpatterns = [
    path("", view_todolist, name="view_todolist"),
    path("<int:id>", view_task, name="view_task"),
    path("create/", create_view, name="create_view"),
    path("<int:id>/edit/", edit_list, name="edit_list"),
    path("<int:id>/delete/", delete_list, name="delete_list"),
    path("items/create", create_task, name="newtask"),
    path("items/<int:id>/edit", update_item, name="update_item"),
]
